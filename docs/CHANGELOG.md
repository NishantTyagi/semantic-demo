# [3.6.0](https://gitlab.com/NishantTyagi/semantic-demo/compare/v3.5.0...v3.6.0) (2020-12-17)


### Features

* new log statement ([eb0ee14](https://gitlab.com/NishantTyagi/semantic-demo/commit/eb0ee141ddcdb0d9315ae4b90dcc1143d8e1dfec))

# [3.5.0](https://gitlab.com/NishantTyagi/semantic-demo/compare/v3.4.1...v3.5.0) (2020-12-17)


### Features

* new feature added ([93fa53c](https://gitlab.com/NishantTyagi/semantic-demo/commit/93fa53cb7791450f2c2b88355c74cee0090d2bb4))

## [3.4.1](https://gitlab.com/NishantTyagi/semantic-demo/compare/v3.4.0...v3.4.1) (2020-12-17)


### Bug Fixes

* log statement added ([4caac45](https://gitlab.com/NishantTyagi/semantic-demo/commit/4caac45c0b39bca630e562bc46b0367db01d0a81))

# [3.4.0](https://gitlab.com/NishantTyagi/semantic-demo/compare/v3.3.0...v3.4.0) (2020-12-17)


### Features

* new feature added ([34ce074](https://gitlab.com/NishantTyagi/semantic-demo/commit/34ce074332203a932e50d87441e258c8f59e0d78))

# [3.3.0](https://gitlab.com/NishantTyagi/semantic-demo/compare/v3.2.0...v3.3.0) (2020-12-17)


### Features

* added npm publish ([740ff2c](https://gitlab.com/NishantTyagi/semantic-demo/commit/740ff2c7a179c4ee70e33f8560b5a6bf2f4b5856))

# [3.2.0](https://gitlab.com/NishantTyagi/semantic-demo/compare/v3.1.1...v3.2.0) (2020-12-17)


### Features

* fixed another bug ([4fc4663](https://gitlab.com/NishantTyagi/semantic-demo/commit/4fc466359e58800a0a2352087dc9b1142977a969))

## [3.1.1](https://gitlab.com/NishantTyagi/semantic-demo/compare/v3.1.0...v3.1.1) (2020-12-17)


### Bug Fixes

* fixed important bug ([6b9cb9d](https://gitlab.com/NishantTyagi/semantic-demo/commit/6b9cb9d63b4353a10079de039aa9344392b9c322))
* removed 3rd log statement ([27cd641](https://gitlab.com/NishantTyagi/semantic-demo/commit/27cd6419cfa6ae44b142e226d6712ee632280f94))

# [3.1.0](https://gitlab.com/NishantTyagi/semantic-demo/compare/v3.0.0...v3.1.0) (2020-12-17)


### Bug Fixes

* removed logs ([7cf2c9e](https://gitlab.com/NishantTyagi/semantic-demo/commit/7cf2c9ee512e6cf552faf8c778da91b981b72e41))
* removed variables ([4f9b123](https://gitlab.com/NishantTyagi/semantic-demo/commit/4f9b123f4d80b5d1b10d2e0823729971be93dd9b))


### Features

* **function:** removed welcome function ([b2367dc](https://gitlab.com/NishantTyagi/semantic-demo/commit/b2367dc9a741e3f8a028d53da5094e590e1ddeb6))

# [3.0.0](https://gitlab.com/NishantTyagi/semantic-demo/compare/v2.0.0...v3.0.0) (2020-12-17)


### Bug Fixes

* fixed name variable ([f675b6a](https://gitlab.com/NishantTyagi/semantic-demo/commit/f675b6a6cb24bb53bbf5269db183a59940a8c2a8))
* fixed name variable 2 ([018c03b](https://gitlab.com/NishantTyagi/semantic-demo/commit/018c03b0d9eebee8d6be68267349f04c0ed3012a))


### Features

* new function added ([8bb13f6](https://gitlab.com/NishantTyagi/semantic-demo/commit/8bb13f694789fd3190ed26292f87c0a142185022))
* variable n added ([cbae688](https://gitlab.com/NishantTyagi/semantic-demo/commit/cbae68872a57a254004f0f99947064258b30578d))

# [2.0.0](https://gitlab.com/NishantTyagi/semantic-demo/compare/v1.7.0...v2.0.0) (2020-12-17)

# [1.7.0](https://gitlab.com/NishantTyagi/semantic-demo/compare/v1.6.0...v1.7.0) (2020-12-17)


### Features

* removed one statement ([d82a0de](https://gitlab.com/NishantTyagi/semantic-demo/commit/d82a0de01e9871af4c8277d6def6966880169080))
* removed one statement 2 ([d0892c3](https://gitlab.com/NishantTyagi/semantic-demo/commit/d0892c3b281744219cc7bb0c790f241dfcc8e3da))

# [1.6.0](https://gitlab.com/NishantTyagi/semantic-demo/compare/v1.5.1...v1.6.0) (2020-12-17)


### Bug Fixes

* added new log ([40b7d92](https://gitlab.com/NishantTyagi/semantic-demo/commit/40b7d92cd9f6e2d392b76f828549280d3dd8cbb7))
* removed variable ([93a6c5b](https://gitlab.com/NishantTyagi/semantic-demo/commit/93a6c5be59ea522889c8cf42d5aa772a27726315))
* removed variable 2 ([18336e9](https://gitlab.com/NishantTyagi/semantic-demo/commit/18336e9a8b7a0245d4cc01cc0ed28dac433ad2a7))


### Features

* added new statements ([87475ae](https://gitlab.com/NishantTyagi/semantic-demo/commit/87475aead347702d1eb2dbba2ab57e954ba5e5ac))

## [1.5.1](https://gitlab.com/NishantTyagi/semantic-demo/compare/v1.5.0...v1.5.1) (2020-12-17)


### Bug Fixes

* added a variable ([6589ed3](https://gitlab.com/NishantTyagi/semantic-demo/commit/6589ed3ce36a31f37c97964e202f56c160c59075))
* added a variable 2 ([1d46f2f](https://gitlab.com/NishantTyagi/semantic-demo/commit/1d46f2faded7436190524a4b1d7f49885c615030))
* fixed random.js ([abe02d4](https://gitlab.com/NishantTyagi/semantic-demo/commit/abe02d4e30e2518ec5512bd27adbc0ed79f22bcd))
* removed some logging ([c4013cd](https://gitlab.com/NishantTyagi/semantic-demo/commit/c4013cdd66b666b69c53c6647a7ac7c785ec03fb))

# [1.5.0](https://gitlab.com/NishantTyagi/semantic-demo/compare/v1.4.0...v1.5.0) (2020-12-17)


### Features

* added random.js ([53b1128](https://gitlab.com/NishantTyagi/semantic-demo/commit/53b1128fd13ff7d488df4308bfc064ee7666831e))
* random text added ([dee55be](https://gitlab.com/NishantTyagi/semantic-demo/commit/dee55be293e56a3d02aaacc3891ee2fb124a7a8e))
* random text added 2 ([53c8a17](https://gitlab.com/NishantTyagi/semantic-demo/commit/53c8a173a75569de90b90207afef992c6d0a11d7))

# [1.4.0](https://gitlab.com/NishantTyagi/semantic-demo/compare/v1.3.0...v1.4.0) (2020-12-17)


### Bug Fixes

* added hello logging ([fdf997b](https://gitlab.com/NishantTyagi/semantic-demo/commit/fdf997b2ebd9d181d63b3f567d440d3653b0b88a))


### Features

* variables removed from auth.js ([2ee7bef](https://gitlab.com/NishantTyagi/semantic-demo/commit/2ee7bef5c0117d537c79cad9d6387df658d48088))

# [1.3.0](https://gitlab.com/NishantTyagi/semantic-demo/compare/v1.2.0...v1.3.0) (2020-12-17)


### Bug Fixes

* changes to auth.js ([ae77c45](https://gitlab.com/NishantTyagi/semantic-demo/commit/ae77c451c6e50284ea96698a86f0e549b8d309cf))
* function added to user.js ([6ac7481](https://gitlab.com/NishantTyagi/semantic-demo/commit/6ac7481ffde57573445c381cd7c2ff5c8cb5aba7))


### Features

* authentication logic added ([e43972a](https://gitlab.com/NishantTyagi/semantic-demo/commit/e43972af074be143454dcded160c3da62e90db57))
* logging statements removed ([432a1f0](https://gitlab.com/NishantTyagi/semantic-demo/commit/432a1f0c62eb8da6a687d00fad77a0c08b102db1))

# [1.2.0](https://gitlab.com/NishantTyagi/semantic-demo/compare/v1.1.0...v1.2.0) (2020-12-16)


### Bug Fixes

* added variable ([b986eef](https://gitlab.com/NishantTyagi/semantic-demo/commit/b986eefb30941f34783a9fcc54e62b68c60aaf18))
* log statement added ([bb2dfd9](https://gitlab.com/NishantTyagi/semantic-demo/commit/bb2dfd99756efbdec0e5f50bfa06fca120a79b75))


### Features

* index.js removed ([ef6b401](https://gitlab.com/NishantTyagi/semantic-demo/commit/ef6b401fbee272e9bfd5f7245417f3c15583b36c))

# [1.1.0](https://gitlab.com/NishantTyagi/semantic-demo/compare/v1.0.0...v1.1.0) (2020-12-16)


### Features

* added index.js ([c3b662c](https://gitlab.com/NishantTyagi/semantic-demo/commit/c3b662c8630c424d48fabed1eed7cdd0b8539be9))

# 1.0.0 (2020-12-16)


### Features

* first publish ([1293dd4](https://gitlab.com/NishantTyagi/semantic-demo/commit/1293dd41c75cf9dedcb8656d92f43a49ba8987a9))
